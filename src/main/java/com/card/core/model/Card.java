package com.card.core.model;

import com.card.core.enums.Rank;
import com.card.core.enums.Suit;

public record Card(Rank rank, Suit suit) {
    @Override
    public String toString() {
        return rank.getSymbol() + "" + suit.getSymbol();
    }
}

package com.card.core.model;

import com.card.core.enums.Rank;
import com.card.core.enums.Suit;

import java.util.Arrays;

public class Deck {
    private Card[] cards;
    private int currentCard;

    public static Deck standard() {
        return new Deck(Rank.TWO);
    }

    public static Deck truncated() {
        return new Deck(Rank.SIX);
    }

    private Deck(Rank startsFrom) {
        var deckSize = (Rank.values().length - startsFrom.ordinal()) * Suit.values().length;
        cards = new Card[deckSize];

        var ranks = Arrays.stream(Rank.values())
                .skip(startsFrom.ordinal())
                .toList();

        currentCard = 0;
        for (Suit suit : Suit.values()) {
            for (Rank rank : ranks) {
                cards[currentCard++] = new Card(rank, suit);
            }
        }
        currentCard--;
    }

    public int size() {
        return currentCard + 1;
    }

    public Card deal() {
        if (size() == 0) return null;
        return cards[currentCard--];
    }

    public Card[] deal(int number) {
        number = Math.min(number, size());
        Card[] dealtCards = new Card[number];
        for (int i = 0; i < number; i++) {
            dealtCards[i] = deal();
        }
        return dealtCards;
    }

    public void shuffle() {
        for (int i = 0; i < size(); i++) {
            int j = (int) (Math.random() * size());
            Card temp = cards[i];
            cards[i] = cards[j];
            cards[j] = temp;
        }
    }

    public void cut() {
        int cut = (int) (Math.random() * size());
        Card[] temp = new Card[size()];
        for (int i = 0; i < size(); i++) {
            temp[i] = cards[(i + cut) % (size())];
        }
        this.cards = temp;
    }

}

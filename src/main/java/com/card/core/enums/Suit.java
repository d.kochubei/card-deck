package com.card.core.enums;

public enum Suit {
    CLUBS('♣'), DIAMONDS('♦'), HEARTS('♥'), SPADES('♠');

    private final Character symbol;

    Suit(Character symbol) {
        this.symbol = symbol;
    }

    public Character getSymbol() {
        return symbol;
    }
}

package com.card.core.model;

import static org.assertj.core.api.Assertions.*;

import com.card.core.enums.Rank;
import com.card.core.enums.Suit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

class DeckTest {
    private Deck deck;

    @BeforeEach
    void setUp() {
        deck = Deck.standard();
    }

    @Test
    void standard_cardsAreUnique() {
        var set = new HashSet<Card>();
        while (deck.size() > 0) set.add(deck.deal());
        assertThat(set.size()).isEqualTo(52);
    }

    @Test
    void standard_containsAllRanksAndSuits() {
        var set = new HashSet<Card>();
        while (deck.size() > 0) set.add(deck.deal());
        assertThat(set).map(Card::rank).contains(Rank.values());
        assertThat(set).map(Card::suit).contains(Suit.values());
    }

    @Test
    void truncated_doesNotHaveCardsLessThanSix() {
        var truncatedDeck = Deck.truncated();
        var set = new HashSet<Card>();
        while (truncatedDeck.size() > 0) set.add(truncatedDeck.deal());
        assertThat(set).map(Card::rank).doesNotContain(Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE);
        assertThat(set).map(Card::suit).contains(Suit.values());
    }

    @Test
    void size_whenDeckIsFull_sizeIs52() {
        int size = deck.size();
        assertThat(size).isEqualTo(52);
    }

    @Test
    void size_whenCardDeal_decreasedByOne() {
        deck.deal();
        assertThat(deck.size()).isEqualTo(51);

        deck.deal();
        assertThat(deck.size()).isEqualTo(50);
    }

    @Test
    void size_whenDeckIsEmpty_sizeIsZero() {
        deck.deal(52);
        assertThat(deck.size()).isEqualTo(0);
    }

    @Test
    void deal_whenDeckIsEmpty_returnNull() {
        deck.deal(52);
        assertThat(deck.deal()).isNull();
    }

    @Test
    void deal_whenDeckIsNotEmpty_returnCardFromTop() {
        assertThat(deck.deal()).isEqualTo(new Card(Rank.ACE, Suit.SPADES));
        assertThat(deck.deal()).isEqualTo(new Card(Rank.KING, Suit.SPADES));
    }

    @Test
    void deal_whenDealMultipleCards_returnCardsFromTop() {
        Card[] cards = deck.deal(4);
        assertThat(cards).containsExactly(
                new Card(Rank.ACE, Suit.SPADES),
                new Card(Rank.KING, Suit.SPADES),
                new Card(Rank.QUEEN, Suit.SPADES),
                new Card(Rank.JACK, Suit.SPADES)
        );
    }

    @Test
    void deal_whenDealMoreCardsThanDeckSize_returnAllCards() {
        Card[] cards = deck.deal(60);
        assertThat(cards).hasSize(52);
    }

    @Test
    void deal_whenDealMultipleCardFromEmptyDeck_returnEmptyArray() {
        deck.deal(52);
        Card[] cards = deck.deal(10);
        assertThat(cards).isEmpty();
    }

    @Test
    void shuffle_sizeIsNotChanged() {
        deck.shuffle();
        assertThat(deck.size()).isEqualTo(52);
    }

    @Test
    void shuffle_whenDeckIsEmpty_doNothing() {
        deck.deal(deck.size());
        deck.shuffle();
        assertThat(deck.size()).isZero();
    }

    @Test
    void shuffle_whenDeckIsShuffled_returnDifferentCard() {
        Card card = deck.deal();
        deck.shuffle();
        assertThat(deck.deal()).isNotEqualTo(card);
    }

    @Test
    void shuffle_whenDeckIsShuffled_cardsAreUnique() {
        deck.shuffle();
        var set = new HashSet<Card>();
        while (deck.size() > 0) set.add(deck.deal());
        assertThat(set).hasSize(52);
    }

    @Test
    void cut_whenDeckIsCut_sizeIsNotChanged() {
        deck.cut();
        assertThat(deck.size()).isEqualTo(52);
    }

    @Test
    void cut_whenDeckIsEmpty_doNothing() {
        deck.deal(deck.size());
        deck.cut();
        assertThat(deck.size()).isZero();
    }

    @Test
    void cut_whenDeckIsCut_cardsAreUnique() {
        deck.cut();
        var set = new HashSet<Card>();
        while (deck.size() > 0) set.add(deck.deal());
        assertThat(set.size()).isEqualTo(52);
    }

    @Test
    void cut_whenDeckIsCutOnce_firstAndLastCardsAreNeighbours() {
        deck.cut();
        var firstCard = deck.deal();
        deck.deal(50);
        var lastCard = deck.deal();

        assertThat(firstCard).isNotEqualTo(lastCard);
        assertThat(firstCard.suit()).isEqualTo(lastCard.suit());
        assertThat(lastCard.rank().ordinal()).isCloseTo(firstCard.rank().ordinal(), within(1));
    }

}